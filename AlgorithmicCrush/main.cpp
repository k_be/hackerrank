//
//  main.m
//  AlgorithmicCrush
//
//  Created by Andrew Romanov on 22/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//

#import <iostream>
#import <vector>
#import <memory>
#import <algorithm>
#import <limits>
#include <functional>

/*
 https://www.hackerrank.com/challenges/crush?h_r=next-challenge&h_v=zen
 */

typedef int64_t Number_t;
typedef std::vector<Number_t> NumbersList_t;

int main(int argc, const char * argv[]) {
	
	// insert code here...
	size_t countItems = 0 ;
	std::cin >> countItems;
	
	std::shared_ptr<NumbersList_t> numbersList(new NumbersList_t(countItems, Number_t(0)));
	
	size_t countOperations = 0;
	std::cin >> countOperations;
	
	for (size_t operationIndex = 0; operationIndex < countOperations; operationIndex++)
	{
		size_t fromIndex;
		std::cin >> fromIndex;
		fromIndex --;
		
		size_t toIndex;
		std::cin >> toIndex;
		
		Number_t value = 0;
		std::cin >> value;
		
		numbersList->at(fromIndex) += value;
		if (toIndex != numbersList->size())
		{
			numbersList->at(toIndex) -= value;
		}
		
	}
	
	Number_t maxElement = 0;
	Number_t sum = 0;
	for (NumbersList_t::const_iterator it = numbersList->cbegin(); it != numbersList->cend(); it++)
	{
		sum += *it;
		maxElement = std::max(maxElement, sum);
	}
	
	std::cout<< maxElement;
	
	return 0;
}

/*
 Объяснение работы алгоритма.
 
 рассмотрим схему сложения для массива из 10 чисел:
 **************
     3 3 3 3
 1 1 1
 0 0 0 0 0 0 0 0 0 0 <- массив
				 3 3 3 3
             4 4 4 4
 2 3 3 3 3 3 1 1
 ***************
 
 
 
 
*/
