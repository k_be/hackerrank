//
//  InsertionSorter.hpp
//  HackerRank
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#ifndef InsertionSorter_hpp
#define InsertionSorter_hpp

#include <stdio.h>
#include <functional>
#include "VectorReader.hpp"
#include <algorithm>


namespace hr {
	template <class T>
	class InsertionSorter{
	public:
		using List = ElementsList<T>;
		using ListIterator = typename List::iterator;
		using SizeType = typename List::size_type;
		using CallbackFunction = std::function<void (size_t step, const ElementsList<T>*)>;
		
		virtual ~InsertionSorter(){};
		
		inline virtual void sortElements(ElementsList<T>& list, CallbackFunction callback)
		{
			_list = &list;
			
			int step = 0;
			for (SizeType iteration = 0; iteration < list.size(); iteration ++)
			{
				ListIterator wrongOrderElement = findNotCorrectedOrderElement();
				if (wrongOrderElement == list.end())
				{
					break;
				}
				
				ListIterator rightPlaceForElement = findRightPlaceForElement(wrongOrderElement);
				T cachedValue = *wrongOrderElement;
				shiftElementsBeatwinIterator(rightPlaceForElement, wrongOrderElement, [=, &step] (void){
					if (callback)
					{
						
						callback(step, _list);
					}
					step++;
				});
				
				*rightPlaceForElement = cachedValue;
				callback(step, _list);
				step ++;
			}
		}
		
		inline ListIterator findNotCorrectedOrderElement() const
		{
			ListIterator findedElement = _list->end();
			
			for (ListIterator index = (_list->begin() + 1); index != _list->end(); index++)
			{
				T previousValue = *(index - 1);
				if (previousValue > *index)
				{
					findedElement = index;
					break;
				}
			}
			
			return findedElement;
		}
		
		
		inline ListIterator findRightPlaceForElement(ListIterator element) const
		{
			ListIterator rightPlace = (element - 1);
			while (*rightPlace > *element && rightPlace != _list->begin())
			{
				rightPlace--;
			}
			
			if (*rightPlace < *element)
			{
				rightPlace = rightPlace + 1;
			}
			
			return rightPlace;
		}
		
		
		inline void shiftElementsBeatwinIterator(ListIterator from, ListIterator to, std::function<void (void)> stepCallback)
		{
			for (ListIterator index = to; index > from; index--)
			{
				*index = *(index - 1);
				if (stepCallback)
				{
					stepCallback();
				}
			}
		}
		
		
	protected:
		ElementsList<T>* _list;
	};
}


#endif /* InsertionSorter_hpp */
