//
//  main.cpp
//  InsertionSortPart1
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#include <iostream>
#include "VectorReader.hpp"
#include "Printers.hpp"
#include "InsertionSorter.hpp"

/*
 input
 10
 2 3 4 5 6 7 8 9 10 1
 
 output 
 2 3 4 5 6 7 8 9 10 10
 2 3 4 5 6 7 8 9 9 10
 2 3 4 5 6 7 8 8 9 10
 2 3 4 5 6 7 7 8 9 10
 2 3 4 5 6 6 7 8 9 10
 2 3 4 5 5 6 7 8 9 10
 2 3 4 4 5 6 7 8 9 10
 2 3 3 4 5 6 7 8 9 10
 2 2 3 4 5 6 7 8 9 10
 1 2 3 4 5 6 7 8 9 10
 */

int main(int argc, const char * argv[]) {
	
	std::size_t countElements = hr::VectorReader<std::size_t>::readValue();
	hr::ElementsList<int> elements = hr::VectorReader<int>::readVectorWithCount(countElements);
	//std::cout<<elements <<"\n";
	hr::InsertionSorter<int> sorter;
	sorter.sortElements(elements, [](size_t step, const hr::ElementsList<int>* list){
		if (step != 0)
		{
			std::cout<<"\n";
		}
		
		std::cout<<*list;
	});
	
	return 0;
}
