//
//  VectorReader.hpp
//  HackerRank
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#ifndef VectorReader_hpp
#define VectorReader_hpp

#include <iostream>
#include <vector>

namespace hr {
	
	template <class T>
	using ElementsList = std::vector<T>;
	
	template <class T>
	class VectorReader
	{
	public:
		
		inline static ElementsList<T> readVectorWithCount(std::size_t countElements)
		{
			ElementsList<T> elements(countElements);
			
			for (std::size_t index = 0; index < countElements; index++)
			{
				std::cin>>elements[index];
			}
			
			return elements;
		}
		
		
		inline static T readValue(void)
		{
			T value;
			std::cin >> value;
			
			return value;
		}
		
	};
}


#endif /* VectorReader_hpp */
