//
//  Printers.hpp
//  HackerRank
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#ifndef Printers_hpp
#define Printers_hpp

#include <iostream>
#include "VectorReader.hpp"

template <class T>
std::ostream& operator<<(std::ostream& stream, const hr::ElementsList<T>& elements){
	
	size_t size = elements.size();
	for (int index = 0; index < size; index++)
	{
		if (index != 0)
		{
			stream << " ";
		}
		stream << elements[index];
	}
	
	return stream;
}


#endif /* Printers_hpp */
