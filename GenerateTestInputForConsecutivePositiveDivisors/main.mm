//
//  main.m
//  GenerateTestInputForConsecutivePositiveDivisors
//
//  Created by Andrew Romanov on 26/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameplayKit/GameplayKit.h>
#import <iostream>
#import <string>
#import <fstream>


int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		std::string fileName = "Input1.txt";
		
		uint64_t countNumbers = UINT64_C(1000000);
		std::ofstream outFile(fileName);
		
		outFile << countNumbers << "\n";
		GKRandomSource* source = [[GKRandomSource alloc] init];
		GKRandomDistribution* uniformDistrimution = [[GKRandomDistribution alloc] initWithRandomSource:source
																																											 lowestValue:3
																																											highestValue:1000000];
		
		for (uint64_t numIndex = 0; numIndex < countNumbers; numIndex++)
		{
			uint64_t randomValue = [uniformDistrimution nextInt];
			outFile<<randomValue;
			if (numIndex != countNumbers - 1)
			{
				outFile << "\n";
			}
			
		}
		
		outFile.close();
	};
	return 0;
}
