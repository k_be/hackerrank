//
//  main.m
//  ConsecutivePpositiveDivisors
//
//  Created by Andrew Romanov on 26/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//

#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <set>
#include <map>


/*
 https://www.hackerrank.com/contests/projecteuler/challenges/euler179
*/

typedef uint64_t Number_t;
typedef std::set<Number_t> Numbers_t;


class CombinationPrivider{
public:
	typedef std::vector<size_t> Indexes_t;
	CombinationPrivider(size_t countElements, size_t maxIndex)
	:_countElements(countElements), _maxIndex(maxIndex), _first(true), _existNext(true), _indexes(countElements, 0)
	{}
	
	Indexes_t next()
	{
		if (!_first)
		{
			bool added = false;
			size_t index = 0;
			while (!added && index != _indexes.size())
			{
				if (_indexes[index] < _maxIndex)
				{
					_indexes[index]++;
					added = true;
				}
				else
				{
					_indexes[index] = 0;
					index++;
				}
			}
			_existNext = (index != _indexes.size());
		}
		_first = false;
		
		return _indexes;
	}
	
private:
	size_t _countElements;
	size_t _maxIndex;
	
	Indexes_t _indexes;
	
	bool _first;
	bool _existNext;
};



Numbers_t primeNumbers;
void fillPrimeNumbersBefore(Number_t topBorder)
{
	primeNumbers.insert(2);
	Number_t currentNumber(3);
	while (currentNumber <= topBorder)
	{
		bool isPrime = true;
		Numbers_t::const_iterator iterator = primeNumbers.begin();
		Number_t primeNumber = *iterator;
		while (isPrime && primeNumber * primeNumber <= currentNumber && iterator != primeNumbers.cend())
		{
			isPrime = (currentNumber % primeNumber  != 0);
			iterator++;
			if (iterator != primeNumbers.cend())
			{
				primeNumber = *iterator;
			}
		}
		
		if (isPrime)
		{
			primeNumbers.insert(primeNumbers.cend(), currentNumber);
		}
		
		currentNumber++;
	}
}


bool isNumberInPrimesList(Number_t number)
{
	bool in = (primeNumbers.find(number) != primeNumbers.cend());
	return in;
}


Number_t countDevidersForNumber(Number_t number)
{
	typedef std::vector<size_t> Deviders_t;
	static Deviders_t primeDeviders(primeNumbers.size() + 1);
	primeDeviders.assign(primeDeviders.size(), 1);
	std::fill(primeDeviders.begin(), primeDeviders.end(), 1);
	Number_t maxPrimeNumber = *(primeNumbers.crbegin());
	while (number != 1 &&(number > maxPrimeNumber || !isNumberInPrimesList(number)))
	{
		Number_t border = std::sqrt(number);
		Numbers_t::const_iterator primeNumberIt = primeNumbers.cbegin();
		Numbers_t::const_iterator endPrime = primeNumbers.cend();
		for (; primeNumberIt != endPrime && *primeNumberIt <= border; primeNumberIt++)
		{
			Number_t primeNum = *primeNumberIt;
			
			if (number % primeNum == 0)
			{
				size_t primeDeviderIndex = std::distance(primeNumbers.cbegin(), primeNumberIt);
				primeDeviders[primeDeviderIndex] += 1;
				number = number / primeNum;
				break;
			}
		}
		
		if (primeNumberIt == primeNumbers.cend() || *primeNumberIt > border)
		{
			//it is a prime number greater prime numbers list
			Deviders_t::reverse_iterator iterator = primeDeviders.rbegin();
			*iterator += 1;
			break;
		}
	}
	if (primeNumbers.find(number) != primeNumbers.cend())
	{
		//it is a prime number greater prime numbers list
		Deviders_t::reverse_iterator iterator = primeDeviders.rbegin();
		*iterator += 1;
	}
	
	Number_t countDeviders(1);
	size_t count = primeDeviders.size();
	for (size_t index = 0; index < count; index++)
	{
		size_t countDeviders = primeDeviders[index];
		if (countDeviders != 0)
		{
			countDeviders *= (countDeviders);
		}
	}
	//countDeviders --;//remove 0
	
	return countDeviders;
}


uint64_t countNumbersBefore(uint64_t topBorder)
{
	uint64_t count = 0;
	
	uint64_t countPrev = 1; //for number = 2;
	for (uint64_t num = 3; num <= topBorder; num++)
	{
		uint64_t countDev = countDevidersForNumber(num);
		if (countPrev == countDev)
		{
			//std::cout<<"equal devider on "<<num - 1<<"and "<<num<<"\n";
			count++;
		}
		
		countPrev = countDev;
	}
	
	return count;
}


void solveWithInputStream(std::istream& inStream)
{
	uint64_t countNumbers = 0;
	inStream >> countNumbers;
	
	fillPrimeNumbersBefore((sqrt(UINT64_C(10000000) + 1)));
	
	for (uint64_t q = 0; q < countNumbers; q++)
	{
		uint64_t num = 0;
		inStream >> num;
		uint64_t count = countNumbersBefore(num);
		std::cout<<count<<"\n";
	}
}


int main(int argc, const char * argv[]) {
	std::ifstream input("Input1.txt");
	solveWithInputStream(input);
	input.close();
	
	return 0;
}
