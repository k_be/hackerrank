//
//  InsertionSorter2.hpp
//  HackerRank
//
//  Created by Andrew Romanov on 07/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#ifndef InsertionSorter2_hpp
#define InsertionSorter2_hpp

#include "InsertionSorter.hpp"


namespace hr {
	template <class T>
	class InsertionSorter2 : public InsertionSorter<T>
	{
	public:
		inline virtual void sortElements(ElementsList<T>& list, typename InsertionSorter<T>::CallbackFunction callback)
		{
			InsertionSorter<T>::_list = &list;
			
			int step = 0;
			for (typename InsertionSorter<T>::SizeType iteration = 1; iteration < list.size(); iteration ++)
			{
				
				
				typename InsertionSorter<T>::ListIterator currentElement = this->_list->begin() + iteration;
				
				if (*currentElement < *(currentElement - 1))
				{
					using ListIterator = typename InsertionSorter<T>::ListIterator;
					ListIterator rightPlaceForElement = this->findRightPlaceForElement(currentElement);
					T cachedValue = *currentElement;
					this->shiftElementsBeatwinIterator(rightPlaceForElement, currentElement, nullptr);
					
					*rightPlaceForElement = cachedValue;
				}
				
				if (callback)
				{
					callback(step, this->_list);
				}
				step ++;
			}
		}	
	};
}

#endif /* InsertionSorter2_hpp */
