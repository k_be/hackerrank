//
//  main.cpp
//  InsertionSortPart2
//
//  Created by Andrew Romanov on 07/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#include <iostream>
#include "VectorReader.hpp"
#include "Printers.hpp"
#include "InsertionSorter2.hpp"


/*
 6
 1 4 3 5 6 2
 
 1 4 3 5 6 2
 1 3 4 5 6 2
 1 3 4 5 6 2
 1 3 4 5 6 2
 1 2 3 4 5 6
 */

int main(int argc, const char * argv[]) {
	
	std::size_t countElements = hr::VectorReader<std::size_t>::readValue();
	hr::ElementsList<int> elements = hr::VectorReader<int>::readVectorWithCount(countElements);
	//std::cout<<elements <<"\n";
	hr::InsertionSorter<int>* sorter = new hr::InsertionSorter2<int>();
	sorter->sortElements(elements, [](size_t step, const hr::ElementsList<int>* list){
		if (step != 0)
		{
			std::cout<<"\n";
		}
		
		std::cout<<*list;
	});
	delete sorter;
	sorter = nullptr;
	
	return 0;
}

