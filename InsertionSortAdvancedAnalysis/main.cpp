//
//  main.cpp
//  InsertionSortAdvancedAnalysis
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
// https://www.hackerrank.com/challenges/insertion-sort?h_r=next-challenge&h_v=zen

#include <iostream>
#include <vector>
#include <algorithm>


typedef std::vector<int> ElementsList;


long countShiffleForSortingElements(const ElementsList& list)
{
	ElementsList sortedList = list;
	std::sort(list.begin(), list.end());
	
	return 0;
}


ElementsList readElements(int count)
{
	ElementsList list(count);
	
	for (int i = 0; i < count; i++)
	{
		std::cin>>list[i];
	}
	
	return list;
}


int readCountQueries(void)
{
	int countQueries = 0;
	std::cin >> countQueries;
	
	return countQueries;
}


int main(int argc, const char * argv[])
{
	int countQueries = readCountQueries();
	for (int queryIndex = 0; queryIndex < countQueries; queryIndex++)
	{
		int countElements = 0;
		std::cin>>countElements;
		ElementsList elements = readElements(countElements);
		long count = countShiffleForSortingElements(elements);
		if (queryIndex != 0)
		{
			std::cout<<"\n";
		}
		std::cout<<count;
	}
	
  return 0;
}
