//
//  main.m
//  FindTheMedian
//
//  Created by Andrew Romanov on 06/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#include <iostream>
#include <vector>
#include <algorithm>


template <class T>
T getMiddleElement(const std::vector<T> values)
{
	std::size_t countElements = values.size();
	std::size_t index = (countElements - 1) / 2;
	T element = values.at(index);
	return element;
}


template <class T>
T findMedian(std::vector<T>& values){
	
	std::sort(values.begin(), values.end());
	T mediana = getMiddleElement(values);
	return mediana;
}


int main(int argc, const char * argv[]) {
	
	std::size_t countElements;
	std::cin >> countElements;
	
	std::vector<int> elements;
	for (std::size_t i = 0; i < countElements; i++)
	{
		int value;
		std::cin>>value;
		elements.push_back(value);
	}
	
	int mediana = findMedian(elements);
	std::cout<<mediana;
	
  return 0;
}
