//
//  main.m
//  CheckPrimeNumber
//
//  Created by Andrew Romanov on 26/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
	@autoreleasepool {
		
		int value = -256;
		uint byteValue = value;
		NSLog(@"byte value = %@", @(byteValue));
		int otherValue = byteValue;
		NSLog(@"other value = %@", @(otherValue));
		
		NSLog(@"interpreatation = %@", @((int)((uint)value)));
		
		
		
		UInt64 primeNumber = UINT64_C(3163);
		
		UInt64 devider = 3;
		UInt64 topBorder = primeNumber;//sqrt(primeNumber);
		BOOL prime = YES;
		while (prime && devider < topBorder)
		{
			prime = (primeNumber % devider != 0);
			if(prime)
			{
				devider ++ ;
			}
		}
		
		if (prime)
		{
			NSLog(@"is prime number");
		}
		else
		{
			NSLog(@"not a prime number with devider = %@", @(devider));
		}
	}
	return 0;
}
