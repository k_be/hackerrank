//
//  main.m
//  BigSorting
//
//  Created by Andrew Romanov on 21/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//

#include <string>
#include <iostream>
#include <set>
#include <fstream>


/*
 https://www.hackerrank.com/challenges/big-sorting/problem
 
 Consider an array of numeric strings, , where each string is a positive number with anywhere from  to  digits. Sort the array's elements in non-decreasing (i.e., ascending) order of their real-world integer values and print each element of the sorted array on a new line.
 
 Input Format
 
 The first line contains an integer, , denoting the number of strings in .
 Each of the  subsequent lines contains a string of integers describing an element of the array.
 
 Constraints
 
 Each string is guaranteed to represent a positive integer without leading zeros.
 The total number of digits across all strings in  is between  and  (inclusive).
 Output Format
 
 Print each element of the sorted array on a new line.
 
 Sample Input 0
 
 6
 31415926535897932384626433832795
 1
 3
 10
 3
 5
 Sample Output 0
 
 1
 3
 3
 5
 10
 31415926535897932384626433832795
 */

struct Container
{
	Container(const std::string& str)
	:string(str), count(0)
	{}
	
	std::string string;
	mutable size_t count;
};


class ContainerComparator
{
public:
	bool operator () (const Container& string1, const Container& string2)
	{
		/*if ((string2.string == "21" && string1.string == "11") ||
				(string1.string == "21" && string2.string == "11"))
		{
			std::cout<<"hi";
		}*/
		
		size_t length1 = string1.string.length();
		size_t length2 = string2.string.length();
		
		bool acceding = length1 < length2;
		if (!acceding)
		{
			if (length1 == length2)
			{
				for (size_t i = 0; i < string1.string.length(); i++)
				{
					char character1 = string1.string[i];
					char character2 = string2.string[i];
					
					if (character1 != character2)
					{
						acceding = character1 < character2;
						break;
					}
				}
			}
		}
		
		return acceding;
	}
};

void solveUsingInput(std::istream& inputStream, std::ostream& outStream)
{
	uint count = 0;
	inputStream >> count;
	
	typedef std::set<Container, ContainerComparator> StringsList_t;
	StringsList_t sortedStrings;
	
	for (uint i = 0; i < count; i++)
	{
		std::string line;
		inputStream >> line;
		std::pair<StringsList_t::iterator,bool> insertedFlag = sortedStrings.emplace(line);
		if (!insertedFlag.second)
		{
			size_t count = insertedFlag.first->count + 1;
			const Container& cont = *(insertedFlag.first);
			cont.count = count;
		}
	}
	
	for (StringsList_t::const_iterator it = sortedStrings.cbegin(); it != sortedStrings.cend(); it++)
	{
		for (int i = 0; i <= it->count; i++)
		{
			outStream << it->string;
			
			if (i != it->count)
			{
				outStream << "\n";
			}
		}
		
		
		if (std::next(it) != sortedStrings.cend())
		{
			outStream << "\n";
		}
	}
}


int main(int argc, const char * argv[]) {
	
	std::ifstream stream("Input2");
	if (stream.is_open())
	{
		solveUsingInput(stream, std::cout);
		stream.close();
	}
	else
	{
		std::cout << "can't open file \n";
	}
	
	return 0;
}
