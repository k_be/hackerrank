#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

/*
 6
 5 4 4 2 2 8
 
 6
 4
 2
 1
 */


bool allEqual(const std::vector<int>& lengths)
{
	bool allEqual = (lengths.size() == 1) || std::equal(lengths.begin() + 1, lengths.end(), lengths.begin());
	
	return allEqual;
}


int main() {
	int countLines = 0;
	std::cin>>countLines;
	
	std::vector<int> lengths;
	for (int i = 0; i < countLines; i++)
	{
		int value = 0;
		std::cin>>value;
		lengths.push_back(value);
	}
	
	
	while (!allEqual(lengths))
	{
		std::cout<<lengths.size()<<"\n";
		std::vector<int>::const_iterator minElement = std::min_element(lengths.begin(), lengths.end());
		int minValue = *minElement;
		std::vector<int>::iterator changedEnd = std::remove(lengths.begin(), lengths.end(), minValue);
		lengths.resize(std::distance(lengths.begin(), changedEnd));
		
		for (std::vector<int>::iterator element = lengths.begin(); element != lengths.end(); element++)
		{
			*element = *element - minValue;
		}
	}
	std::cout<<lengths.size()<<"\n";
  
  return 0;
}
