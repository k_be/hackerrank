//
//  main.m
//  WhiteFalconAndTree
//
//  Created by Andrew Romanov on 23/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//
#include <vector>
#include <memory>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <cmath>

/*
 https://www.hackerrank.com/challenges/white-falcon-and-tree
*/

typedef int64_t Number_t;
typedef size_t NodeIndex_t;

class LinieFunction
{
public:
	typedef std::shared_ptr<LinieFunction> SPtr;
	
	LinieFunction()
	:a(0), b(0), base(1000000007)
	{}
	
	LinieFunction(Number_t a, Number_t b)
	:a(a), b(b), base(1000000007)
	{}
	
  inline Number_t solveForX(Number_t x)
	{
		Number_t result = ((a * (x % base)) % base + b) % base;
		return result;
	}
	
	inline Number_t operator () (Number_t x)
	{
		return solveForX(x);
	}
	
	SPtr concatinateAfterFunction(LinieFunction firstFunction)
	{
		SPtr concatination(new LinieFunction((a * firstFunction.a) % base, ((a * firstFunction.b) % base + b % base) % base));
		return concatination;
	}
protected:
	Number_t a;
	Number_t b;
	
	Number_t base;
};


//********
template <class Data_t>
class Node {
public:
	typedef std::vector<Node> List_t;
	typedef std::vector<const Node*> ListP_t;
	
	Node(NodeIndex_t index)
	:_index(index), _parent(nullptr)
	{}
	
	inline const size_t getIndex() const
	{
		return _index;
	}
	
	Data_t getData() const
	{
		return _data;
	}
	
	void setData(const Data_t& data)
	{
		_data = data;
	}
	
	Node* getParent() const
	{
		return _parent;
	}
	
	void setParent(Node* parent)
	{
		_parent = parent;
	}
private:
	NodeIndex_t _index;
	Node* _parent;
	
	Data_t _data;
};


typedef std::pair<NodeIndex_t, NodeIndex_t> Edge_t;//first - from, second - to
typedef std::vector<Edge_t> EdgesList_t;

bool operator < (const Edge_t& left, const Edge_t& right)
{
	bool less = left.first < right.first;
	
	if (!less && left.first == right.first)
	{
		less = left.second < right.second;
	}
	
	return less;
}


inline EdgesList_t reverceEdgesList(const EdgesList_t& originalList)
{
	EdgesList_t reversedList;
	
	for (EdgesList_t::const_reverse_iterator it = originalList.crbegin(); it != originalList.crend(); it++)
	{
		reversedList.push_back(Edge_t(it->second, it->first));
	}
	
	return reversedList;
}


typedef std::vector<NodeIndex_t> IndexesList_t;
typedef IndexesList_t Path_t;
IndexesList_t indexesFromPath(const EdgesList_t& path)
{
	IndexesList_t list;
	if (path.size() == 1)
	{
		list.push_back(path.cbegin()->first);
		if (path.cbegin()->first != path.cbegin()->second)
		{
			list.push_back(path.cbegin()->second);
		}
	}
	else if (path.size() > 1)
	{
		list.push_back(path.crbegin()->first);
		for (EdgesList_t::const_reverse_iterator edge = std::next(path.crbegin()); edge != path.crend(); edge++)
		{
			list.push_back(edge->first);
			if (std::next(edge) == path.crend())
			{
				list.push_back(edge->second);
			}
		}
	}
	
	return list;
}


//********
class Tree {
public:
	typedef Node<LinieFunction::SPtr> Node_t;
	typedef std::vector<Node_t*> NodePList_t;
	
	Tree(size_t countNodes)
	{
		for (size_t index = 0; index < countNodes; index++)
		{
			Node_t* node = new Node_t(index);
			_nodesList.push_back(node);
		}
	}
	
	~Tree()
	{
		std::for_each(_nodesList.begin(), _nodesList.end(), [](Node_t*& node){
			delete node;
			node = nullptr;
		});
	}
	
	void addEdge(const Edge_t& edge)
	{
		Node_t* from = _nodesList.at(edge.first);
		Node_t* to = _nodesList.at(edge.second);
		
		if (to->getParent() == nullptr)
		{
			to->setParent(from);
		}
		else
		{
			from->setParent(to);
		}
	}
	
	
	inline IndexesList_t findPath(NodeIndex_t from, NodeIndex_t to)
	{
		IndexesList_t fromIndexes;
		IndexesList_t toIndexes;
		
		Node_t* currentNode1 = _nodesList.at(from);
		Node_t* currentNode = _nodesList.at(to);
		
		IndexesList_t::const_iterator commonElementInFrom = fromIndexes.cend();
		IndexesList_t::const_iterator commonElementInTo = toIndexes.cend();
		do{
			IndexesList_t::const_iterator lastInTo = toIndexes.cend();
			if (currentNode != nullptr)
			{
				NodeIndex_t index = currentNode->getIndex();
				toIndexes.push_back(index);
				currentNode = currentNode->getParent();
				lastInTo = std::prev(toIndexes.cend());
			}
				
			IndexesList_t::const_iterator lastInFrom = fromIndexes.cend();
			if (currentNode1 != nullptr)
			{
				fromIndexes.push_back(currentNode1->getIndex());
				currentNode1 = currentNode1->getParent();
				
				lastInFrom = std::prev(fromIndexes.cend());
			}
			
			if (lastInTo != toIndexes.cend())
			{
				commonElementInFrom  = std::find(fromIndexes.cbegin(), fromIndexes.cend(), *lastInTo);
				if(commonElementInFrom != fromIndexes.cend())
				{
					commonElementInTo = lastInTo;
				}
				else
				{
					commonElementInTo = toIndexes.cend();
				}
			}
			else
			{
				commonElementInTo = std::find(toIndexes.cbegin(), toIndexes.cend(), * lastInFrom);
				if (commonElementInTo != toIndexes.cend())
				{
					commonElementInFrom = lastInFrom;
				}
				else
				{
					commonElementInFrom = fromIndexes.cend();
				}
			}
		}while (commonElementInFrom == fromIndexes.cend() &&
						(currentNode != nullptr ||
						currentNode1 != nullptr));
		
		fromIndexes.erase(commonElementInFrom, fromIndexes.end());
		for (IndexesList_t::const_reverse_iterator toIt = toIndexes.crbegin(); toIt != toIndexes.crend(); toIt++)
		{
			fromIndexes.push_back(*toIt);
		}
		
		return fromIndexes;
	}
	
	
	/*
	 path starts from last edge
	 */
	typedef std::function<void (Node_t*)> Function;
	inline void applyToPath(NodeIndex_t currentIndex, NodeIndex_t toIndex, Function fun = nullptr)
	{
		IndexesList_t path = findPath(currentIndex, toIndex);
		for (IndexesList_t::const_iterator it = path.cbegin(); it != path.cend(); it++)
		{
			if(fun)
			{
				fun(_nodesList.at(*it));
			}
		}
	}
	
	
	inline void applyConcatenationForNodesAtPath(NodeIndex_t fromIndex, NodeIndex_t toIndex, const LinieFunction::SPtr& firstFun)
	{
		applyToPath(fromIndex, toIndex, [&](Node_t* node){
			LinieFunction::SPtr concatenation = node->getData()->concatinateAfterFunction(*firstFun);
			node->setData(concatenation);
		});
	}
	
	
	inline void replaceFunctionsAtPath(NodeIndex_t fromIndex, NodeIndex_t toIndex, const LinieFunction::SPtr& fun)
	{
		applyToPath(fromIndex, toIndex, [&](Node_t* node){
			node->setData(fun);
		});
	}
	
	
	inline Number_t calculateConcatenationValueAtPath(NodeIndex_t fromIndex, NodeIndex_t toIndex, Number_t x)
	{
		Number_t result = x;
		applyToPath(fromIndex, toIndex, [&](Node_t* node){
			result = (*node->getData())(result);
		});
		
		return result;
	}
	
	
	inline void setFunctionToNode(NodeIndex_t nodeIndex, const LinieFunction::SPtr& fun)
	{
		Node_t* node = _nodesList.at(nodeIndex);
		node->setData(fun);
	}
	
private:
	inline constexpr static NodeIndex_t maxNodeIndex()
	{
		return std::numeric_limits<NodeIndex_t>::max();
	}
private:
	NodePList_t _nodesList;
	NodeIndex_t _rootNodeIndex;
};


std::ostream& operator << (std::ostream& outstream, const Edge_t& edge)
{
	outstream << "("<<edge.first<<","<<edge.second<<")";
	return outstream;
}


std::ostream& operator << (std::ostream& outStream, const Path_t& path)
{
	if (path.size() != 0)
	{
		for(Path_t::const_iterator pathIterator = path.cbegin(); pathIterator != path.cend(); pathIterator++)
		{
			outStream << *pathIterator;
			if(std::next(pathIterator) != path.cend())
			{
				outStream << "->";
			}
		}
	}
	
	return outStream;
}


void solveWithInput(std::istream& inputStream)
{
	size_t countVertexes;
	inputStream >> countVertexes;
	
	Tree tree(countVertexes);
	
	for (size_t nodeIndex = 0; nodeIndex < (countVertexes); nodeIndex ++)
	{
		Number_t a;
		Number_t b;
		inputStream>> a >> b;
		tree.setFunctionToNode(nodeIndex, LinieFunction::SPtr(new LinieFunction(a, b)));
		
	}
	
	for (size_t edgeIndex = 0; edgeIndex < (countVertexes - 1); edgeIndex ++)
	{
		Edge_t edge(0, 0);
		inputStream >> edge.first;
		inputStream >> edge.second;
		edge.first--;
		edge.second--;
		
		tree.addEdge(edge);
	}
	
	size_t countQueries = 0;
	inputStream >> countQueries;
	
	bool printed = false;
	for (size_t queryIndex = 0; queryIndex < countQueries; queryIndex ++)
	{
		size_t queeryType = 0;
		inputStream >> queeryType;
		if (queeryType == 1)
		{
			NodeIndex_t from;
			inputStream >> from;
			from --;
			NodeIndex_t to;
			inputStream >> to;
			to --;
			Number_t a;
			inputStream >> a;
			Number_t b;
			inputStream >> b;
			
			tree.replaceFunctionsAtPath(from, to, LinieFunction::SPtr(new LinieFunction(a, b)));
		}
		else
		{
			NodeIndex_t from;
			inputStream >> from;
			from --;
			NodeIndex_t to;
			inputStream >> to;
			to --;
			Number_t x;
			inputStream >> x;
			
			Number_t result = tree.calculateConcatenationValueAtPath(from, to, x) %  (1000000007);
			if (printed)
			{
				std::cout<<"\n";
			}
			printed = true;
			std::cout<<result;
		}
	}
	
}


int main(int argc, const char * argv[]) {
	std::ifstream file("Input1");
	solveWithInput(file);
	file.close();
	
	std::ifstream treeS("Tree1");
	
	size_t countVertexes;
	treeS >> countVertexes;
	
	Tree tree(countVertexes);
	for (size_t edgeIndex = 0; edgeIndex < (countVertexes - 1); edgeIndex ++)
	{
		Edge_t edge(0, 0);
		treeS >> edge.first;
		treeS >> edge.second;
		edge.first--;
		edge.second--;
		
		tree.addEdge(edge);
	}
	
	NodeIndex_t from = 6;
	NodeIndex_t to = 4;
	std::cout<<"path from " << from << " to "<< to << " : "<<tree.findPath(from, to) << "\n";
	
	
	return 0;
}
