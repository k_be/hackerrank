//
//  main.cpp
//  ReadFromFile
//
//  Created by Andrew Romanov on 11/10/2016.
//  Copyright © 2016 Home. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>


using String = std::string;

int main(int argc, const char * argv[]) {
	// insert code here...

	std::ifstream inputFileStream("Input.txt");
	int count;
	inputFileStream>>count;
	inputFileStream.ignore(1, '\n');
	
	for(int i = 0; i < count; i++)
	{
		String line;
		std::getline(inputFileStream, line);
		
		std::istringstream lineStream(line);
		
		
		String nameOfGame;
		String leastFav;
		String fav;
		String bestCharacter;
		
		std::getline(lineStream, leastFav, '|');
		std::getline(lineStream, nameOfGame, '|');
		std::getline(lineStream, fav, '|');
		std::getline(lineStream, bestCharacter, '|');
		
		std::cout<< leastFav <<"\n"<<nameOfGame<<"\n"<<fav<<"\n"<<bestCharacter<<"\n\n";
	}
	
  return 0;
}
