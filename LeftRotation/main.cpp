//
//  main.m
//  LeftRotation
//
//  Created by Andrew Romanov on 22/06/2017.
//  Copyright © 2017 Home. All rights reserved.
//
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

/*
 https://www.hackerrank.com/challenges/array-left-rotation?h_r=next-challenge&h_v=zen
 
 A left rotation operation on an array of size  shifts each of the array's elements  unit to the left. For example, if left rotations are performed on array , then the array would become .
 
 Given an array of  integers and a number, , perform  left rotations on the array. Then print the updated array as a single line of space-separated integers.
 
 Input Format
 
 The first line contains two space-separated integers denoting the respective values of  (the number of integers) and  (the number of left rotations you must perform).
 The second line contains  space-separated integers describing the respective elements of the array's initial state.
 
 Constraints
 
 Output Format
 
 Print a single line of  space-separated integers denoting the final state of the array after performing  left rotations.
*/

typedef uint64_t Number_t;
typedef std::vector<uint64_t> NumbersList_t;


void solveWithStream(std::istream& stream)
{
	size_t countNumbers = 0;
	stream >> countNumbers;
	size_t countShifts = 0;
	stream >> countShifts;
	if (countNumbers > 0)
	{
		countShifts = countShifts % countNumbers;
		NumbersList_t numbers(countNumbers);
		
		for (size_t i = 0; i < countNumbers; i++)
		{
			size_t destinationIndex = (i + countNumbers - countShifts) % countNumbers;
			stream >> numbers.at(destinationIndex) ;
		}
		
		for (NumbersList_t::const_iterator it = numbers.cbegin(); it != numbers.cend(); it++)
		{
			std::cout<<*it;
			if (std::next(it) != numbers.cend())
			{
				std::cout<<" ";
			}
		}
	}
}

int main(int argc, const char * argv[]) {
	
	std::ifstream inputStream("Input1");
	solveWithStream(inputStream);
	inputStream.close();
	return 0;
}
